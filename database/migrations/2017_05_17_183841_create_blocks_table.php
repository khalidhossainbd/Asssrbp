<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('block_name');
            $table->integer('upozilla_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->integer('division_id')->unsigned();

            $table->foreign('upozilla_id')->references('id')->on('upozillas');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('division_id')->references('id')->on('divisions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocks');
    }
}
