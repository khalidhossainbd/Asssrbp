<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUpozillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upozillas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('upozilla_name');
            $table->integer('dist_id')->unsigned();

            $table->foreign('dist_id')->references('id')->on('districts');
            $table->timestamps();
        });
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upozillas');
    }
}
