<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('farmer_name');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('husbend_name');
            $table->string('division');
            $table->string('district');
            $table->string('upozilla');
            $table->string('Category');
            $table->string('block');
            $table->string('group');
            $table->string('union');
            $table->string('village');
            $table->string('farmer_type');
            $table->string('education');
            $table->integer('age');
            $table->integer('land');
            $table->string('gender');
            $table->string('occupation');
            $table->integer('national_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('farmers');
    }
}
