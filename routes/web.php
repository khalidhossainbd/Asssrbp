<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'MainController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/userregister', 'HomeController@userregister')->name('userregister');
Route::get('/admin/userlist', 'HomeController@userlist')->name('userlist');

route::get('/admin/find_dist', 'HomeController@find_dist');
route::get('/admin/find_upozilla', 'HomeController@find_upozilla');

Route::resource('admin/divisions', 'Admin\\DivisionsController');
Route::resource('admin/districts', 'Admin\\DistrictsController');
Route::resource('admin/upozillas', 'Admin\\UpozillasController');
Route::resource('admin/farmers', 'Admin\\FarmersController');
Route::resource('admin/blocks', 'Admin\\BlocksController');