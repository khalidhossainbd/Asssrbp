<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'districts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['district_name', 'div_id'];

    public function division(){
        return $this->belongsTo('App\Division');
    }

    public function upozilla(){
        return $this->hasMany('App\Upozilla');
    }
    public function block(){
        return $this->hasMany('App\Block');
    }

    
}
