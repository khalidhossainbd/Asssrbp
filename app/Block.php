<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['block_name', 'upozilla_id','district_id','division_id'];

    public function upozilla(){
        return $this->belongsTo('App\Upozilla');
    }
    public function district(){
        return $this->belongsTo('App\District');
    }
    public function division(){
        return $this->belongsTo('App\Division');
    }

        
}
