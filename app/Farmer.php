<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'farmers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['farmer_name', 'father_name', 'mother_name', 'husbend_name', 'division', 'district', 'upozilla', 'block', 'category', 'group', 'union', 'village', 'farmer_type', 'education', 'age', 'land', 'gender', 'occupation', 'national_id'];

    
}
