<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upozilla extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'upozillas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['upozilla_name', 'dist_id'];

    public function district(){
        return $this->belongsTo('App\District');
    }

    public function block(){
        return $this->hasMany('App\Block');
    }

    
}
