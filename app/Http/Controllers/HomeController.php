<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Block;

use Session;
use App\User;
use App\Upozilla;
use App\Division;
use App\District;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    public function userregister()
    {
        return view('admin.register');
    }

    public function userlist()
    {
        $data = User::all();
        return view('admin.userlist', compact('data'));
    }

    public function find_dist(Request $request)
    {
        $data = District::select('id', 'district_name')->where('div_id', $request->id)->get();

        return response()->json($data);
    }

    public function find_upozilla(Request $request)
    {
       $data = Upozilla::select('id', 'upozilla_name')->where('dist_id', $request->id)->get();
       return response()->json($data); 
    }
}
