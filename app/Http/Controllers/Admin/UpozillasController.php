<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Upozilla;

use App\District;

use Illuminate\Http\Request;
use Session;

class UpozillasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $upozillas = Upozilla::where('upozilla_name', 'LIKE', "%$keyword%")
				->orWhere('dist_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $upozillas = Upozilla::paginate($perPage);
        }

        return view('admin/upozilla.upozillas.index', compact('upozillas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $districts = District::all();
        return view('admin/upozilla.upozillas.create', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Upozilla::create($requestData);

        Session::flash('flash_message', 'Upozilla added!');

        return redirect('admin/upozillas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $upozilla = Upozilla::findOrFail($id);

        return view('admin/upozilla.upozillas.show', compact('upozilla'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $upozilla = Upozilla::findOrFail($id);

        $districts = District::all();
        return view('admin/upozilla.upozillas.edit', compact('upozilla', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $upozilla = Upozilla::findOrFail($id);
        $upozilla->update($requestData);

        Session::flash('flash_message', 'Upozilla updated!');

        return redirect('admin/upozillas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Upozilla::destroy($id);

        Session::flash('flash_message', 'Upozilla deleted!');

        return redirect('admin/upozillas');
    }
}
