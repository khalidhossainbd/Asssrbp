<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Block;
use Illuminate\Http\Request;
use Session;
use App\Upozilla;
use App\Division;
use App\District;
use Illuminate\Support\Facades\DB;

class BlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blocks = Block::with('upozilla','district','division')
            ->where('block_name', 'LIKE', "%$keyword%")
				->orWhere('upozilla_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $blocks = Block::with('upozilla','district','division')
            ->paginate($perPage);
        }

        // $upozilla = Upozilla::all();

        return view('admin/block.blocks.index', compact('blocks', 'upozilla'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $divisions = Division::all();
        $districts = District::all();
        $upozillas = Upozilla::all();
        return view('admin/block.blocks.create', compact('upozillas', 'districts', 'divisions' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
         //dd($request);
        $requestData = $request->all();
        
        Block::create($requestData);

        Session::flash('flash_message', 'Block added!');

        return redirect('admin/blocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $block = Block::findOrFail($id);

        return view('admin/block.blocks.show', compact('block'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $block = Block::findOrFail($id);

        $divisions = Division::all();
        $districts = DB::table('districts')->where('div_id', $block->division_id)->get();
        $upozillas = DB::table('upozillas')->where('dist_id', $block->district_id)->get();

        return view('admin/block.blocks.edit', compact('block', 'divisions', 'districts', 'upozillas' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        $block = Block::findOrFail($id);
        $block->update($requestData);

        Session::flash('flash_message', 'Block updated!');

        return redirect('admin/blocks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Block::destroy($id);

        Session::flash('flash_message', 'Block deleted!');

        return redirect('admin/blocks');
    }
}
