<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\District;

use App\Division;

use Illuminate\Http\Request;
use Session;

class DistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $districts = District::where('district_name', 'LIKE', "%$keyword%")
				->orWhere('div_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $districts = District::paginate($perPage);
        }
        
        return view('admin/disarea.districts.index', compact('districts', 'divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $divisions = Division::all();
        return view('admin/disarea.districts.create', compact('divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        District::create($requestData);

        Session::flash('flash_message', 'District added!');

        return redirect('admin/districts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
      
        $district = District::findOrFail($id);
              // dd($district);
        return view('admin/disarea.districts.show', compact('district'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $district = District::findOrFail($id);
        $divisions = Division::all();

        return view('admin/disarea.districts.edit', compact('district', 'divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $district = District::findOrFail($id);
        $district->update($requestData);

        Session::flash('flash_message', 'District updated!');

        return redirect('admin/districts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        District::destroy($id);

        Session::flash('flash_message', 'District deleted!');

        return redirect('admin/districts');
    }
}
