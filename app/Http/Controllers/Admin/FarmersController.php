<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Farmer;
use Illuminate\Http\Request;
use Session;

class FarmersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $farmers = Farmer::where('farmer_name', 'LIKE', "%$keyword%")
				->orWhere('father_name', 'LIKE', "%$keyword%")
				->orWhere('mother_name', 'LIKE', "%$keyword%")
				->orWhere('husbend_name', 'LIKE', "%$keyword%")
				->orWhere('division', 'LIKE', "%$keyword%")
				->orWhere('district', 'LIKE', "%$keyword%")
				->orWhere('upozilla', 'LIKE', "%$keyword%")
				->orWhere('block', 'LIKE', "%$keyword%")
				->orWhere('group', 'LIKE', "%$keyword%")
				->orWhere('union', 'LIKE', "%$keyword%")
				->orWhere('village', 'LIKE', "%$keyword%")
				->orWhere('farmer_type', 'LIKE', "%$keyword%")
				->orWhere('education', 'LIKE', "%$keyword%")
				->orWhere('age', 'LIKE', "%$keyword%")
				->orWhere('land', 'LIKE', "%$keyword%")
				->orWhere('gender', 'LIKE', "%$keyword%")
				->orWhere('occupation', 'LIKE', "%$keyword%")
				->orWhere('national_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $farmers = Farmer::paginate($perPage);
        }

        return view('admin/farmers.farmers.index', compact('farmers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin/farmers.farmers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // dd($request);
        $requestData = $request->all();
        
        Farmer::create($requestData);

        Session::flash('flash_message', 'Farmer added!');

        return redirect('admin/farmers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $farmer = Farmer::findOrFail($id);

        return view('admin/farmers.farmers.show', compact('farmer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $farmer = Farmer::findOrFail($id);

        return view('admin/farmers.farmers.edit', compact('farmer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $farmer = Farmer::findOrFail($id);
        $farmer->update($requestData);

        Session::flash('flash_message', 'Farmer updated!');

        return redirect('admin/farmers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Farmer::destroy($id);

        Session::flash('flash_message', 'Farmer deleted!');

        return redirect('admin/farmers');
    }
}
