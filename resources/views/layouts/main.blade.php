<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Md Khalid Hossain">
	<meta http-equiv="refresh" content="300">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/bootstrap/css/bootstrap-theme.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/bootstrap/css/bootstrap-theme.css.map') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/bootstrap/css/bootstrap.min.css.map') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/navber/css/animate.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/assets/navber/css/manu.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/custom.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/mystyle.css') }}">
<!-- <a href=""><img class="img-responsive" src="dist/img/bd_logo.png" alt="Bangladesh Govt Logo" style="max-height: 100px;"></a> -->


</head>
<body>

@include('partiles.main_header')

@yield('content')
	
@include('partiles.main_footer')

	<script type="text/javascript" src="{{ asset('dist/assets/jquery/jquery-3.2.1.js')}}"></script>
	<script type="text/javascript" src="{{ asset('dist/assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<!-- <script type="text/javascript" src="dist/assets/bootstrap/js/npm.js"></script> -->
	<script type="text/javascript" src="{{ asset('dist/assets/navber/js/manu.js')}}"></script>
	<script type="text/javascript" src="{{ asset('dist/assets/js/myjs.js')}}"></script>
	
	<script type="text/javascript">
		
		$(document).ready( function() {
    $('#myCarousel').carousel({
    	interval:   4000
	});
	
	var clickEvent = false;
	$('#myCarousel').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});
	</script>
</body>
</html>





