@extends('layouts.dashboard')

@section('content')

        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Icons</li>
            </ol>
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add New Farmer</h1>
            </div>
        </div><!--/.row-->
                
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    <a href="{{ url('/admin/farmers') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back Farmer List</button></a>
                    </div>

                    <div class="panel panel-warning">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>

                    <div class="panel-body">
                        
                        {!! Form::open(['url' => '/admin/farmers', 'role' => 'form', 'files' => true]) !!}
                            <div class="col-md-5">
                                <div class="form-group {{ $errors->has('farmer_name') ? 'has-error' : ''}}">
                                    <label>Farmer Name</label>
                                    <input name="farmer_name" class="form-control" placeholder="Name" required>
                                </div>
                                <div class="form-group{{ $errors->has('father_name') ? 'has-error' : ''}}">
                                    <label>Father Name</label>
                                    <input name="father_name" class="form-control" placeholder=" Father Name">
                                </div>
                                <div class="form-group {{ $errors->has('mother_name') ? 'has-error' : ''}}">
                                    <label>Mother Name</label>
                                    <input name="mother_name" class="form-control" placeholder="Mother Name">
                                </div>
                                <div class="form-group {{ $errors->has('husbend_name') ? 'has-error' : ''}}">
                                    <label>Husband Name</label>
                                    <input name="husbend_name" class="form-control" placeholder="Husband Name">
                                </div>

                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios1" value="Male" checked>Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios2" value="Female">Female
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('age') ? 'has-error' : ''}}">
                                    <label>Age</label>
                                    <input name="age" class="form-control" placeholder="Farmer Age">
                                </div>
                                <div class="form-group {{ $errors->has('national_id') ? 'has-error' : ''}}">
                                    <label>National Id</label>
                                    <input name="national_id" class="form-control" placeholder="National Id" required>
                                </div>
                                <div class="form-group {{ $errors->has('occupation') ? 'has-error' : ''}}">
                                    <label>Occupation</label>
                                    <input name="occupation" class="form-control" placeholder="Occupation">
                                </div>
                                <div class="form-group {{ $errors->has('education') ? 'has-error' : ''}}">
                                    <label>Education</label>
                                    <input name="education" class="form-control" placeholder="Education">
                                </div>
                                <div class="form-group {{ $errors->has('land') ? 'has-error' : ''}}">
                                    <label>Land Area</label>
                                    <input name="land" class="form-control" placeholder="Land Area">
                                </div>
                                
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control" name="category" required >
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zone</label>
                                    <select class="form-control" name="division" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Dhaka</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>District</label>
                                    <select class="form-control" name="district" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Upozilla</label>
                                    <select class="form-control" name="upozilla" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Block</label>
                                    <select class="form-control" name="block" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Farmer Group</label>
                                    <select class="form-control" name="group" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group {{ $errors->has('union') ? 'has-error' : ''}}">
                                    <label>Union</label>
                                    <input name="union" class="form-control" placeholder="Union Name" required>
                                </div>
                                <div class="form-group {{ $errors->has('village') ? 'has-error' : ''}}">
                                    <label>Village</label>
                                    <input name="village" class="form-control" placeholder="Village Name" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Farmer Type</label>
                                    <select class="form-control" name="farmer_type" required>
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                    </select>
                                </div>
                                
                                
                                
                                <button type="submit" class="btn btn-primary">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div><!-- /.col-->
        </div><!-- /.row -->
        
    </div><!--/.main-->


    {{-- <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9 pu">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Farmer</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/farmers') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/farmers', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin/farmers.farmers.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
