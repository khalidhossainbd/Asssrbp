<div class="form-group {{ $errors->has('farmer_name') ? 'has-error' : ''}}">
    {!! Form::label('farmer_name', 'Farmer Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('farmer_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('farmer_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('father_name') ? 'has-error' : ''}}">
    {!! Form::label('father_name', 'Father Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('father_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('father_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mother_name') ? 'has-error' : ''}}">
    {!! Form::label('mother_name', 'Mother Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mother_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mother_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('husbend_name') ? 'has-error' : ''}}">
    {!! Form::label('husbend_name', 'Husbend Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('husbend_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('husbend_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('division') ? 'has-error' : ''}}">
    {!! Form::label('division', 'Division', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('division', ['Dhaka', 'Barisal', 'Khulna'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('division', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('district') ? 'has-error' : ''}}">
    {!! Form::label('district', 'District', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('district', ['technology', 'tips', 'health'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('district', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('upozilla') ? 'has-error' : ''}}">
    {!! Form::label('upozilla', 'Upozilla', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('upozilla', ['technology', 'tips', 'health'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('upozilla', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('block') ? 'has-error' : ''}}">
    {!! Form::label('block', 'Block', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('block', ['technology', 'tips', 'health'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('block', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('group') ? 'has-error' : ''}}">
    {!! Form::label('group', 'Group', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('group', ['technology', 'tips', 'health'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('group', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('union') ? 'has-error' : ''}}">
    {!! Form::label('union', 'Union', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('union', null, ['class' => 'form-control']) !!}
        {!! $errors->first('union', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('village') ? 'has-error' : ''}}">
    {!! Form::label('village', 'Village', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('village', null, ['class' => 'form-control']) !!}
        {!! $errors->first('village', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('farmer_type') ? 'has-error' : ''}}">
    {!! Form::label('farmer_type', 'Farmer Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('farmer_type', null, ['class' => 'form-control']) !!}
        {!! $errors->first('farmer_type', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('education') ? 'has-error' : ''}}">
    {!! Form::label('education', 'Education', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('education', null, ['class' => 'form-control']) !!}
        {!! $errors->first('education', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('age') ? 'has-error' : ''}}">
    {!! Form::label('age', 'Age', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('age', null, ['class' => 'form-control']) !!}
        {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('land') ? 'has-error' : ''}}">
    {!! Form::label('land', 'Land', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('land', null, ['class' => 'form-control']) !!}
        {!! $errors->first('land', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('gender', ['male', 'female'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('occupation') ? 'has-error' : ''}}">
    {!! Form::label('occupation', 'Occupation', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('occupation', null, ['class' => 'form-control']) !!}
        {!! $errors->first('occupation', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('national_id') ? 'has-error' : ''}}">
    {!! Form::label('national_id', 'National Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('national_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('national_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
