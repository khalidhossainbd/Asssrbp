

<div class="form-group {{ $errors->has('district_name') ? 'has-error' : ''}}">
    {!! Form::label('district_name', 'District Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('district_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('district_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('div_id') ? 'has-error' : ''}}">
    {!! Form::label('div_id', 'Division Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control" name="div_id">
            @foreach($divisions as $division)
             <option value="{{ $division->id }}">{{ $division->division_name}}</option>
            @endforeach
        </select>

        {!! $errors->first('div_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
{{-- {!! Form::select('div_id', [

            @foreach($divisions as $division)
                <li>{{ $division->division_name }}</li>
            @endforeach
        ], null, ['class' => 'form-control']) !!} --}}