<div class="form-group {{ $errors->has('upozilla_name') ? 'has-error' : ''}}">
    {!! Form::label('upozilla_name', 'Upozilla Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('upozilla_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('upozilla_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dist_id') ? 'has-error' : ''}}">
    {!! Form::label('dist_id', 'District Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control" name="dist_id">
            @foreach($districts as $district)
             <option value="{{ $district->id }}">{{ $district->district_name}}</option>
            @endforeach
        </select>
        {!! $errors->first('dist_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
