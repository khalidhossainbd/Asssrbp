@extends('layouts.dashboard')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-9 pull-right">
                <div class="panel panel-default">
                    <div class="panel-heading">Blocks List</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/blocks/create') }}" class="btn btn-success btn-sm" title="Add New Block">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/blocks', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">
                                    <i class="fa fa-search"></i>Search
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless table-hover">
                                <thead>
                                    <tr>
                                        <th>Serial No.</th>
                                        <th>Block Name</th>
                                        <th>Upozilla Name</th>
                                        <th>District Name</th>
                                        <th>Division Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i= 1; ?>
                                @foreach($blocks as $item)
                                    <tr>
                                        <td> <?php echo $i++ ;?></td>
                                        <td>{{ $item->block_name }}</td>
                                        <td>{{ $item->upozilla->upozilla_name }}</td>
                                        <td>{{ $item->district->district_name }}</td>
                                        <td>{{ $item->division->division_name }}</td>
                                       {{--  <td>{{ $item->upozilla_id }}</td> --}}
                                        <td>
                                            <a href="{{ url('/admin/blocks/' . $item->id) }}" title="View Block"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/blocks/' . $item->id . '/edit') }}" title="Edit Block"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/blocks', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Block',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $blocks->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
