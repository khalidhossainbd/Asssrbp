<div class="form-group {{ $errors->has('block_name') ? 'has-error' : ''}}">
    {!! Form::label('block_name', 'Block Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('block_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('block_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('upozilla_id') ? 'has-error' : ''}}">
    {!! Form::label('upozilla_id', 'Upozilla Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('upozilla_id', ['technology', 'tips', 'health'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('upozilla_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
