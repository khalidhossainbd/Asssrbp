@extends('layouts.dashboard')

@section('content')

        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Icons</li>
            </ol>
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create Block Area</h1>
            </div>
        </div><!--/.row-->
                
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ url('/admin/blocks') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Block List</button></a>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            {!! Form::open(['url' => '/admin/blocks', 'role' => 'form', 'files' => true]) !!}
                                <div class="form-group">
                                    <label>Zone Name</label>
                                    <select name="division_id" class="form-control demo"  >
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        @foreach($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->division_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>District Name</label>
                                    <div>
                                    <select name="district_id" class="form-control demo1 demo2" >
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        {{-- <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option> --}}
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Upozilla Name</label>
                                    <select name="upozilla_id" class="form-control demo3" required="required">
                                        <option selected disabled="disabled"> --select an option-- </option>
                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Bloak Name</label>
                                    <input name="block_name" class="form-control" placeholder="Bloak Name" required="required">
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                                
                                
                            </div>
                            
                        {!! Form::close() !!}
                    </div>
                </div>
            </div><!-- /.col-->
        </div><!-- /.row -->
        
    </div><!--/.main-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script type="text/javascript">
    

        $(document).ready(function(){
            $(document).on('change', '.demo', function(){
            // console.log('It is ok');
            var div_id = $(this).val();
            // console.log(div_id);
            // var $this = $(this);
            var op =" ";

            $.ajax({
                type:'get',
                url:'{{ url('/admin/find_dist')}}',
                data:{ 'id' : div_id},
                success: function(data){
                    // console.log("ok");
                    // console.log(data.length);
                    op+= '<option value="0" selected disabled >--select an option-- </option>';

                    for (var i = 0; i < data.length; i++) {
                        op+= '<option value="'+ data[i].id +'">'+ data[i].district_name + '</option>';
                        }

                    // $this.closest('.panel-body').find('.demo1').html(op);
                    $('.demo1').html(op);

                    // div.find('.demo1').append(op);

                    // console.log(op);

                },
                error:function(){

                }
            });
            });
        });


        $(document).ready(function(){
            $(document).on('change', '.demo2', function(){
                // console.log('It is ok');
                var dist_id = $(this).val();
                var opp= " ";
                $.ajax({
                type:'get',
                url:'{{ url('/admin/find_upozilla')}}',
                data:{ 'id' : dist_id},
                success: function(data){
                    opp+= '<option value="0" selected disabled >--select an option-- </option>';
                     for (var i = 0; i < data.length; i++) {
                        opp+= '<option value="'+ data[i].id +'">'+ data[i].upozilla_name + '</option>';
                        }
                     $('.demo3').html(opp);
                },
                error:function(){

                }
            });
        });
    });
    </script>

{{-- 
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Block</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/blocks') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/blocks', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin/block.blocks.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
