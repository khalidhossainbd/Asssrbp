@extends('layouts.dashboard')

@section('title', 'Agriculture Dashboard')

@section('content')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">User Register</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">User Register Form </div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" >
							
								<div class="form-group">
									<label>User Name</label>
									<input class="form-control" placeholder="User Name">
								</div>
								<div class="form-group">
									<label>User Type</label>
									<select class="form-control">
										<option>Option </option>
										<option>Super Admin</option>
										<option>Admin</option>
										<option>User</option>
										
									</select>
								</div>
								<div class="form-group">
									<label>Email</label>
									<input type="Email" class="form-control" placeholder="Email">
								</div>
																
								<div class="form-group">
									<label>Password</label>
									<input type="password" class="form-control">
								</div>
								<button type="submit" class="btn btn-primary">Submit Button</button>
								<button type="reset" class="btn btn-default">Reset Button</button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

@endsection
