

@extends('layouts.main')

@section('title', 'Agricultural Support for Smallholders in South-Western Region of Bangladesh Project')

@section('content')
	
	<!-- Session End -->
	<!-- Secession start -->
	<section>
		
		<div class="container">
	    <div id="myCarousel" class="carousel slide" data-ride="carousel">
	        <!-- Wrapper for slides -->
	        <div class="carousel-inner">
	            <div class="item active">
	                <!-- <img src="http://placehold.it/1200x400/16a085/ffffff&text=About Us"> -->
	                <img src="dist/img/Slider/slider01.png" style="min-height: 300px;">
	                <div class="carousel-caption">
	                    <h2>
	                       Agricultural Support for Smallholders in South- Western Region of Bangladesh Project</h2>
	                    <p>
	                        </p>
	                </div>
	            </div>
	            <!-- End Item -->
	            <div class="item">
	                <!-- <img src="http://placehold.it/1200x400/e67e22/ffffff&text=Projects"> -->
	                <img src="dist/img/Slider/slide02.png" style="min-height: 300px;">
	                <div class="carousel-caption">
	                    <h3>
	                        Headline</h3>
	                    <p>
	                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
	                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem
	                        ipsum dolor sit amet, consetetur sadipscing elitr.</p>
	                </div>
	            </div>
	            <!-- End Item -->
	            <div class="item">
	                <!-- <img src="http://placehold.it/1200x400/2980b9/ffffff&text=Portfolio"> -->
	                <img src="dist/img/Slider/slide03.png" style="min-height: 300px;">
	                <div class="carousel-caption">
	                    <h3>
	                        Headline</h3>
	                    <p>
	                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
	                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem
	                        ipsum dolor sit amet, consetetur sadipscing elitr.</p>
	                </div>
	            </div>
	            <!-- End Item -->
	            <div class="item">
	                <!-- <img src="http://placehold.it/1200x400/8e44ad/ffffff&text=Services"> -->
	                <img src="dist/img/Slider/slide04.png" style="min-height: 300px;">
	                <div class="carousel-caption">
	                    <h3>
	                        Headline</h3>
	                    <p>
	                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
	                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem
	                        ipsum dolor sit amet, consetetur sadipscing elitr.</p>
	                </div>
	            </div>
	            <!-- End Item -->
	        </div>
	        <!-- End Carousel Inner -->
	        <ul class="nav nav-pills nav-justified">
	            <li data-target="#myCarousel" data-slide-to="0" class="active"><a href="#"> Project Steering </br> Committee{{-- <small>Lorem ipsum dolor sit</small> --}}</a></li>
	            <li data-target="#myCarousel" data-slide-to="1"><a href="#"> Project Management </br> Committee{{-- <small>Lorem ipsum
	                dolor sit</small> --}}</a></li>
	            <li data-target="#myCarousel" data-slide-to="2"><a href="#">Official & Staff </br>Management {{-- <small>Lorem ipsum dolor sit</small> --}}</a></li>
	            <li data-target="#myCarousel" data-slide-to="3"><a href="#">Farmers Group </br>Management {{-- <small>Lorem ipsum
	                dolor sit</small> --}}</a></li>
	        </ul>
	    </div>
	    <!-- End Carousel -->
		</div>
	</section>
	<!-- Session End -->
	<!-- Secession start -->
	<section>
		<div class="container">
			<div class="allSides">
				<div class="row" style="padding: 30px 0px;">
					<div class="col-md-2" style="margin: 0px 15px;">
				        <div class="report">
				        <a href="{{ url('/monitoring')}}"><h3>Repoting Networks</h3></a>
				        </div>
			        	<div class="thumbnail">
			        	<img src="{{ asset('dist/img/progress.jpg')}}" alt="Progress Report" style="margin: 5px auto;">
				    	</div>
				    	<div class="reporting">
				    		<ul>
			        		<li>
			        			<a href="{{url('/machineries_monitoring')}}">Agricultural Machineries</a>
			        		</li>
			        		<li >
			        			<a href="{{url('/bazar_monitoring')}}">Bazar Construction Monitoring</a>
			        		</li>
			        		<li>
			        			<a href="">Farmer Group Training Module</a>
			        		</li>
			        		<li style="border-bottom: none;">
			        			<a href="">Rice/Cereal Management Module</a>
			        		</li>
			        		</ul>
				    	</div>
					    
				        
				    </div>
				    <div class="col-md-7">
				    <h3>The Project ASSSRBP</h3>
				    <p>
				    	Bangladesh is one of the promising countries in this world. Due to geographical position, South-Western region of comes across a number of natural disasters every year and inflicts huge damages of crops, livestock’s and houses and for her geographical characteristics poverty and malnutrition reminds as critical problem despite an impressive growth in food grain production in the recent years. Agriculture in this area is characterized by low productivity because of salinity problem, lack of saline tolerant varieties, improper soil fertility management, lack of control over water resources and less practice of modern technologies, mono-crop culture and repeated crop losses due to natural calamities. Bangladesh Governments aim is to support rice and other cereals production, promotion of high value vegetables, fruits and other horticulture crops through intensification and diversification program.

				    	<br><br><br>
						The colossal Sidr on 15th November attacked the south and south-western districts of Barisal, Patuakhali, Jhalakathi and Borguna and caused huge loss of lives and properties. After Sidr, ad IDB team visited the affected area and expressed their strong willingness to stand behind the affected people in assisting through a support project. On this background “Agricultural Support for smallholders in South –Western Region of Bangladesh Project (ASSSRBP)’ is launched in 58 Upazilas of 9 districts for the period from July 2013 to June 2018. 

 						
				    </p>
				      
				    </div>
				    
				    <div class="col-md-2 pull-right" style="margin: 0px 15px;">
				        <div class="thumbnail">
				        	<img class="img-pm" src="{{ asset('dist/img/office/pm.jpg') }}" alt="Prime Minister">
				        </div>
				        <div class="caption">
				          <h4>Sheikh Hasina</h4>
				            <p>Prime Minister of Bangladesh</p>
				        </div>
				    	<div class="thumbnail">
				        	<img class="img-pm" src="{{ asset('dist/img/office/head.jpg')}}" alt="Director" >
					    </div>
					    <div class="caption">
					          <h4>Md. Golam Farque</h4>
					            <p>Director <br> Ministry of Agriculture</p>
					    </div>

				        <div>
				        	<h2>Notice</h2>
				        	<ul>
				        		<li>
				        			<p>Notice one is here</p>
				        		</li>
				        		<li>
				        			<p>Notice one is here</p>
				        		</li>
				        		<li>
				        			<p>Notice one is here</p>
				        		</li>
				        		<li>
				        			<p>Notice one is here</p>
				        		</li>
				        	</ul>
				        </div>
				    </div>
				</div>
			</div>
		</div>
		
		
	</section>
	<!-- Session End -->
	<!-- Secession start -->
	<section style="background-image: url('dist/img/back-01.png');">
		<div class="container">
			<div class="row" style="margin: 40px 25px">
				<div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		    	    <!-- <img src="http://lorempixel.com/output/food-q-c-320-240-2.jpg" class="img-responsive"/> -->
		    	    <div class="thumbnail">
		    	    	<img src="{{ asset('') }}dist/img/gellary/g-01.png" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
		    	</div>
		        <div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		            <!-- <img src="http://lorempixel.com/output/food-q-c-320-240-4.jpg" class="img-responsive"/> -->
		            <div class="thumbnail">
		    	    	<img src="{{ asset('') }}dist/img/gellary/g-01.png" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
				</div>
		        <div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		            <!-- <img src="http://lorempixel.com/output/food-q-c-320-240-1.jpg" class="img-responsive"/> -->
		            <div class="thumbnail">
		    	    	<img src="{{ asset('dist/img/gellary/g-01.png') }}" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
				</div>
		        <div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		            <!-- <img src="http://lorempixel.com/output/food-q-c-320-240-1.jpg" class="img-responsive"/> -->
		            <div class="thumbnail">
		    	    	<img src="{{ asset('dist/img/gellary/g-04.png') }}" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
				</div>
		        <div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		        <!--     <img src="http://lorempixel.com/output/food-q-c-320-240-2.jpg" class="img-responsive"/> -->
		            <div class="thumbnail">
		    	    	<img src="{{ asset('dist/img/gellary/g-03.png') }}" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
				</div>
		        <div class="col-md-4 col-sm-4 col-xs-6 shine_me">
		            <!-- <img src="http://lorempixel.com/output/food-q-c-320-240-4.jpg" class="img-responsive"/> -->
		            <div class="thumbnail">
		    	    	<img src="{{ asset('dist/img/gellary/g-02.png') }}" class="img-responsive">
		    	    	<i class="shine_effect"></i>
		    	    </div>
				</div>
			</div>
		</div>
    

	</section>
	<!-- Session End -->
	<!-- Secession start -->
	<section>
		
		<div class="container">
			<div class="thumbnail">
				<img class="img-responsive" src="{{ asset('dist/img/Map.png')}}">
			</div>
			
		</div>
	</section>
	<!-- Session End -->
	<!-- Secession start -->
	<section></section>
	<!-- Session End -->
@endsection