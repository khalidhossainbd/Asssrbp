-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2017 at 07:52 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asssrbp`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `block_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upozilla_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `block_name`, `upozilla_id`, `created_at`, `updated_at`) VALUES
(1, 'Test', 16, '2017-05-20 06:00:08', '2017-05-20 06:00:08'),
(3, 'New test', 13, '2017-05-20 08:12:32', '2017-05-20 08:12:32'),
(5, 'Sadar block', 12, '2017-05-20 08:43:48', '2017-05-20 08:44:12'),
(6, 'Test Block', 12, '2017-05-21 11:05:28', '2017-05-21 11:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(10) UNSIGNED NOT NULL,
  `district_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `div_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `district_name`, `div_id`, `created_at`, `updated_at`) VALUES
(1, 'Bagerhat', 1, '2017-05-17 12:51:17', '2017-05-17 12:51:17'),
(2, 'Gopalgonj', 3, '2017-05-17 12:51:32', '2017-05-17 12:51:32'),
(3, 'Madaripur', 3, '2017-05-17 12:51:44', '2017-05-17 12:51:44'),
(4, 'Barisal', 2, '2017-05-17 12:51:56', '2017-05-17 12:51:56'),
(5, 'Bhola', 2, '2017-05-17 12:52:03', '2017-05-17 12:52:03'),
(6, 'Patuakhali', 2, '2017-05-17 12:52:12', '2017-05-17 12:52:12'),
(7, 'Jhalakathi', 2, '2017-05-17 12:52:28', '2017-05-17 12:52:28'),
(8, 'Pirojpur', 2, '2017-05-17 12:52:37', '2017-05-17 12:52:37'),
(9, 'Barguna', 2, '2017-05-17 12:52:48', '2017-05-17 12:52:48');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `division_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `division_name`, `created_at`, `updated_at`) VALUES
(1, 'Khulna', '2017-05-17 12:49:53', '2017-05-17 12:49:53'),
(2, 'Barisal', '2017-05-17 12:49:59', '2017-05-17 12:49:59'),
(3, 'Faridpur', '2017-05-17 12:50:25', '2017-05-17 12:50:25');

-- --------------------------------------------------------

--
-- Table structure for table `farmers`
--

CREATE TABLE `farmers` (
  `id` int(10) UNSIGNED NOT NULL,
  `farmer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mother_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `husbend_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upozilla` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `union` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `farmer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `land` int(11) NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(14, '2014_10_12_000000_create_users_table', 1),
(15, '2014_10_12_100000_create_password_resets_table', 1),
(16, '2017_05_15_062532_create_divisions_table', 1),
(17, '2017_05_15_063331_create_districts_table', 1),
(18, '2017_05_15_185904_create_upozillas_table', 1),
(19, '2017_05_17_045408_create_farmers_table', 1),
(20, '2017_05_17_183841_create_blocks_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upozillas`
--

CREATE TABLE `upozillas` (
  `id` int(10) UNSIGNED NOT NULL,
  `upozilla_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dist_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upozillas`
--

INSERT INTO `upozillas` (`id`, `upozilla_name`, `dist_id`, `created_at`, `updated_at`) VALUES
(1, 'Gopalganj Sadar', 2, '2017-05-17 12:54:37', '2017-05-17 12:54:37'),
(2, 'Tungipara', 2, '2017-05-17 12:55:19', '2017-05-17 12:55:19'),
(3, 'Barisal Sadar', 4, '2017-05-17 12:55:40', '2017-05-17 12:55:40'),
(4, 'Rajoir', 4, '2017-05-17 12:56:09', '2017-05-17 12:56:09'),
(5, 'Patuakhali Sadar', 6, '2017-05-17 12:56:50', '2017-05-17 12:56:50'),
(6, 'Dumki', 6, '2017-05-17 12:57:05', '2017-05-17 12:57:05'),
(7, 'Rajpur', 7, '2017-05-17 12:57:22', '2017-05-17 12:57:22'),
(8, 'Kathalia', 7, '2017-05-17 12:57:57', '2017-05-17 12:57:57'),
(9, 'Barguna  Sadar', 9, '2017-05-17 12:58:36', '2017-05-17 12:58:36'),
(10, 'Amtali', 9, '2017-05-17 12:58:53', '2017-05-17 12:58:53'),
(11, 'Bhola Sadar', 5, '2017-05-17 12:59:17', '2017-05-17 12:59:17'),
(12, 'Manpura', 5, '2017-05-17 12:59:35', '2017-05-17 12:59:35'),
(13, 'Pirojpur Sadar', 8, '2017-05-17 13:00:06', '2017-05-17 13:00:06'),
(14, 'Nazirpur', 8, '2017-05-17 13:00:27', '2017-05-17 13:00:27'),
(15, 'Bagerhat Sadar', 1, '2017-05-17 13:00:46', '2017-05-17 13:00:46'),
(16, 'Mongla', 1, '2017-05-17 13:01:04', '2017-05-17 13:01:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'khalid Hossain', 'khalid@gmail.com', '$2y$10$r7GFdekivCMmc3YrxzG0SudS2suwCvye9kKo5Dab13YEjBsggXk8y', NULL, '2017-05-18 03:55:18', '2017-05-18 03:55:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocks_upozilla_id_foreign` (`upozilla_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_div_id_foreign` (`div_id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmers`
--
ALTER TABLE `farmers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `upozillas`
--
ALTER TABLE `upozillas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upozillas_dist_id_foreign` (`dist_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `farmers`
--
ALTER TABLE `farmers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `upozillas`
--
ALTER TABLE `upozillas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blocks`
--
ALTER TABLE `blocks`
  ADD CONSTRAINT `blocks_upozilla_id_foreign` FOREIGN KEY (`upozilla_id`) REFERENCES `upozillas` (`id`);

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_div_id_foreign` FOREIGN KEY (`div_id`) REFERENCES `divisions` (`id`);

--
-- Constraints for table `upozillas`
--
ALTER TABLE `upozillas`
  ADD CONSTRAINT `upozillas_dist_id_foreign` FOREIGN KEY (`dist_id`) REFERENCES `districts` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
